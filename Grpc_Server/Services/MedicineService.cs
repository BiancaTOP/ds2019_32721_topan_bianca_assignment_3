﻿using Grpc.Core;
using Grpc_Server.Protos;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grpc_Server.Services
{

    public class MedicineService : Medicines.MedicinesBase
    {
        private readonly ILogger<MedicineService> _logger;

        public MedicineService(ILogger<MedicineService> logger)
        {
            _logger = logger;
        }

        public override Task<MedicinesModel> GetMedicineInfo(MedicinePatientModel request, ServerCallContext context)
        {
            MedicinesModel output = new MedicinesModel();


            if (request.PatientId == 1)
            {
                output.Medicine1 = "Ibuprofen";
                output.Medicine2 = "Nurofen";
                output.Medicine3 = "Vitamax";
                output.Medicine4 = "Paracetamol";

                //output.Medicine1Time = "14:20:00";
                output.Medicine1Time = DateTime.Now.AddHours(-14).ToString();
                output.Medicine2Time = DateTime.Now.AddHours(-8).ToString();
                output.Medicine3Time= DateTime.Now.AddHours(1).AddMinutes(20).ToString();
                output.Medicine4Time = DateTime.Now.AddHours(2).AddMinutes(40).ToString();
            }
            else
            {
                output.Medicine1 = "Ibuprofen";
                output.Medicine2 = "Nurofen";
                output.Medicine3 = "Vitamax";
                output.Medicine4 = "Paracetamol";

                output.Medicine1Time = "14:20";
                output.Medicine2Time = "16:20";
                output.Medicine3Time = "18:20";
                output.Medicine4Time = "20:20";
            }

            return Task.FromResult(output);
        }
    }
}
