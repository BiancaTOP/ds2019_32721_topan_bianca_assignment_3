import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

interface Dispenser {
    //date: string;
    medicine: string,
}

interface Medicine {
    name: string,
    isTaken: boolean,
    time: Date
}


@Component({
  selector: 'app-fetch-data',
  templateUrl: './fetch-data.component.html'
})
export class FetchDataComponent {
    public medicines: Medicine[] = [];
    public DateNow: Date;

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {

      setInterval(() => {
          this.DateNow = new Date();
      }, 1);

      http.get<Dispenser>(baseUrl + 'weatherforecast').subscribe((result: any) => {
          //console.log(result);
          //this.medicines.push(result);
          var med1: Medicine = {
              name: result.medicine1,
              isTaken: result.isMed1Taken,
              time: result.medicine1Time
          };

          var med2: Medicine = {
              name: result.medicine2,
              isTaken: result.isMed2Taken ,
              time: result.medicine2Time
          };

          var med3: Medicine = {
              name: result.medicine3,
              isTaken: result.isMed3Taken ,
              time: result.medicine3Time
          };

          var med4: Medicine = {
              name: result.medicine4,
              isTaken: result.isMed4Taken ,
              time: result.medicine4Time
          };
          this.medicines.push(med1, med2, med3, med4);
      }, error => console.error(error));
    }


    compareDate(time: Date) {
        var comparer = new Date(time);
   
        return this.DateNow.getTime() < comparer.getTime(); 
    }

    isFuture(time: Date) {
        var compare = new Date(time);
        var max = compare.setHours(compare.getHours() + 1);
        console.log(max);
        var maxDate = new Date(max);
        return this.DateNow.getTime() < compare.getTime() && max < compare.getTime(); 
    }

    takeMedicine(medicine: Medicine) {
        this.medicines.filter(med => med == medicine).map(med => med.isTaken = true);
    }
    
}


