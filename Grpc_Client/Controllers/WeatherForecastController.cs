﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Net.Client;
using Grpc_Server;
using Grpc_Server.Protos;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Grpc_Client.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        //public async Task<string> Get()
         public async Task<object> Get()
        {
            //var input = new HelloRequest {Name="Bogdan" };

            //var channel = GrpcChannel.ForAddress("https://localhost:5001");
            //var client = new Greeter.GreeterClient(channel: channel);

            //var reply = await client.SayHelloAsync(input);

            var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var client = new Medicines.MedicinesClient(channel: channel);

            var patientRequested = new MedicinePatientModel { PatientId = 1 };

            var reply = await client.GetMedicineInfoAsync(patientRequested);

            return new {
                IsMed1Taken = reply.IsMed1Taken,
                IsMed2Taken = reply.IsMed2Taken,
                IsMed3Taken = reply.IsMed3Taken,
                IsMed4Taken = reply.IsMed4Taken,
                Medicine1 = reply.Medicine1,
                Medicine2 = reply.Medicine2,
                Medicine3 = reply.Medicine3,
                Medicine4 =reply.Medicine4,
                Medicine1Time = reply.Medicine1Time,
                Medicine2Time = reply.Medicine2Time,
                Medicine3Time = reply.Medicine3Time,
                Medicine4Time = reply.Medicine4Time
            };
        }
    }
}
